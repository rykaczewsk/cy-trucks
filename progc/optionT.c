//PréIng2 MI2 Semestre 1, Projet CY-Trucks : Traitement -t
//#include "data.csv"
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#define SIZE 100

typedef struct _tree {
    char ville[SIZE];    //on classera par ordre alphabétique dans l'AVL: ville FG < ville Racine < ville FD
    int nb_apparition;
    struct _tree* pL;
    struct _tree* pR;
    int equilibre;      //equilibre = hauteur sous-arbre droit - hauteur sous-arbre gauche
} Tree;


Tree* createTree(char nom[]){
    Tree* pNew = malloc(sizeof(Tree));
    if(pNew == NULL){
        printf("Erreur 1 : pointeur NULL.\n");
        exit(1);
    }
    int i = 0;
    while(nom[i] != '\0'){
        if (i >= 99){
            printf("ERREUR 2 : Nom de ville de plus de 99 caractères.\n");
            exit(2);
        }
        pNew->ville[i] = *(nom + i);
        i++;
    }
    pNew->nb_apparition = 1;        //si on crée le noeud, c que le nom de ville vient d'apparaître pour la 1ere fois
    pNew->equilibre = 0;
    pNew->pL = NULL;
    pNew->pR = NULL;
    return pNew;
}

//----------------- ROTATIONS SIMPLES -----------------
Tree* RotationGauche(Tree* p){
    if (p==NULL || p->pR == NULL){
        exit(10);
    }
    Tree* pivot = p->pR;

    //Equilibrage
    if(p->equilibre == 2){
        if(pivot->equilibre == 0){
            pivot->equilibre = -1;
            p->equilibre = 1;
        }
        else if(pivot->equilibre == 1){
            pivot->equilibre = 0;
            p->equilibre = 0;
        }
        else if(pivot->equilibre == 2){
            pivot->equilibre = 0;
            p->equilibre = -1;
        }
        else{
            printf("Cas non pris en charge 11. \n");
            exit(11);
        }
    }

    else if(p->equilibre == 1){
        if(pivot->equilibre == -1){
            pivot->equilibre = -2;
            p->equilibre = 0;
        }
        else if(pivot->equilibre == 0){
            pivot->equilibre = -1;
            p->equilibre = 1;
        }
        else if(pivot->equilibre == 1){
            pivot->equilibre = -1;
            p->equilibre = -1;
        }
        else{
            printf("Cas non pris en charge 12. \n");
            exit(12);
        }
    }

    else{
        printf("Cas non pris en charge 13. \n");
        exit(13);
    }

    //Rotation
    p->pR = pivot->pL;
    pivot->pL = p;
    if (pivot == NULL){
        printf("Erreur 14 : pointeur NULL.\n");
        exit(14);
    }

    return pivot;
}


Tree* RotationDroite(Tree* p){
    if (p==NULL || p->pL == NULL){
        exit(15);
    }
    Tree* pivot = p->pL;

    //Equilibrage
    if(p->equilibre == -2){
        //Rotation simple 1
        if(pivot->equilibre == -1){
            pivot->equilibre = 0;
            p->equilibre = 0;
        }
        //Rotation Simple 2
        else if(pivot->equilibre == 0){
            pivot->equilibre = 1;
            p->equilibre = -1;
        }
        else if(pivot->equilibre == -2){
            pivot->equilibre = -1;
            p->equilibre = -1;
        }
        else{
            printf("Cas non pris en charge 16. \n");
            exit(16);
        }
    }

    else if(p->equilibre == -1){
        if(pivot->equilibre == -1){
            pivot->equilibre = 0;
            p->equilibre = 0;
        }
        else if(pivot->equilibre == 0){
            pivot->equilibre = 1;
            p->equilibre = 0;
        }
        else if(pivot->equilibre == 1){
            pivot->equilibre = 2;
            p->equilibre = 0;
        }
        else{
            printf("Cas non pris en charge 17. \n");
            exit(17);
        }
    }

    else{
        printf("Cas non pris en charge 18. \n");
        exit(18);
    }

    //Rotation
    p->pL = pivot->pR;
    pivot->pR = p;
    if (pivot == NULL){
        printf("Erreur 19 : pointeur NULL.\n");
        exit(19);
    }

    return pivot;
}


//----------------- DOUBLES ROTATIONS -----------------
Tree* DoubleRotationGauche(Tree* p){
    if (p==NULL || p->pR==NULL || p->equilibre!=2 || p->pR->equilibre!=-1){
        printf("Cas non pris en charge 20 : pointeurs NULL ou facteurs d'équilibre incorrectes.\n");
        exit(20);
    }

    p->pR = RotationDroite(p->pR);
    if (p->pR == NULL){
        printf("Erreur 21 : pointeur NULL.\n");
        exit(21);
    }
    p = RotationGauche(p);
    if (p == NULL){
        printf("Erreur 22 : pointeur NULL.\n");
        exit(22);
    }
    return p;
}



Tree* DoubleRotationDroite(Tree* p){
    if (p==NULL || p->pL==NULL || p->equilibre!=-2 || p->pL->equilibre!=1){
        printf("Cas non pris en charge 30 : pointeurs NULL ou facteurs d'équilibre incorrectes.\n");
        exit(30);
    }

    p->pL = RotationGauche(p->pL);
    if (p->pL == NULL){
        printf("Erreur 31 : pointeur NULL.\n");
        exit(31);
    }

    p = RotationDroite(p);
    if (p == NULL){
        printf("Erreur 32 : pointeur NULL.\n");
        exit(32);
    }
    return p;
}

//----------------- EQUILIBRAGE -----------------
Tree* equilibrerAVL(Tree* p){
    if (p == NULL){
        printf("Erreur 40 : pointeur NULL.\n");
        exit(40);
    }

    if (p->equilibre == 2){
        if (p->pR == NULL){
            printf("Erreur 41 : pointeur NULL.\n");
            exit(41);
        }

        //Double rotation Gauche (RD puis RG)
        if (p->pR->equilibre == -1){
            p = DoubleRotationGauche(p);
        }

        //Rotation Simple Gauche
        else if (p->pR->equilibre >= 0){
            p = RotationGauche(p);
        }

        else {
            printf("Cas non pris en charge 42.\n");
            exit(42);
        }
    }
    else if (p->equilibre == -2) {
        if (p->pL == NULL){
            printf("Erreur 43 : pointeur NULL.\n");
            exit(43);
        }

        //Double rotation Droite (RG puis RD)
        if (p->pL->equilibre == 1){
            p = DoubleRotationDroite(p);
        }

        //Rotation Simple Droite
        else if (p->pL->equilibre <= 0){
            p = RotationDroite(p);
        }

        else {
            printf("Cas non pris en charge 44.\n");
            exit(44);
        }
    }
    else{
        //do nothing
        printf("Cas non pris en charge 45 : racine de l'arbre >2 ou <-2 .\n");
        exit(45);
    }
    if (p == NULL){
        printf("Erreur 46 : pointeur NULL.\n");
        exit(46);
    }

    return p;
}


//----------------- INSERTION AVL -----------------
Tree* insertAVL (Tree* p, char nom[], int* h){           //addABR
    //TEST
    if (h == NULL){
        printf("Erreur 50 : pointeur NULL.\n");
        exit(50);
    }

    //process
    if (p == NULL){
        p = createTree(nom);
        *h = 1;
        if (p == NULL){
            printf("Erreur 51 : pointeur NULL.\n");
            exit(51);
        }
    }
    else if (strcmp(nom, p->ville) > 0){
        p->pR = insertAVL(p->pR, nom, h);
    }
    else if (strcmp(nom, p->ville) < 0){
        p->pL = insertAVL(p->pL, nom, h);
        *h = -*h;
    }
    else {
        // On a trouvé le même nom de ville : on ne fait rien
        // strcmp(nom, p->ville) == 0
    }

    if (*h != 0){
        p->equilibre = p->equilibre + *h;
        p = equilibrerAVL(p);
        if(p->equilibre == 0){
            *h = 0;
        }
        else {
            *h = 1;
        }
    }

    // return the address (may be new)
    return p;
}


int main() {
    
    return 0;
}